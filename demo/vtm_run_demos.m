function vtm_run_demos(exitOnFinish)
  if nargin < 1
    exitOnFinish=true;
  end
  
  sep = filesep();
  mypath = mfilename('fullpath');
  [mydir,~,~] = fileparts(mypath);
    
  codeFolder = strcat(mydir, sep, '..', sep, 'tests');
  addpath(codeFolder)
  vtm_test_set_environ()
 
  demos =  {'hello_world', true,
            'single_scalar_field', true};
  
  for i=1:size(demos,1)
    name = demos{i, 1};
    showcode = demos{i, 2};
    fname = ['demo_', name, '.m'];
    close all
    vtm_manage_demos('register', name);
    if exist(fname, 'file') == 2
      publish(fname, struct('showCode', showcode))
    else 
      fprintf(1, 'File %s not found', fname);
      vtm_manage_demos('report', name, false);
    end
    vtm_manage_demos('cleanup', name);
  end

  publish('list_of_demos.m', struct('showCode', false))
  
  if exitOnFinish
    if ~vtm_manage_demos('status') 
      exit(22)
    else
      exit(0)
    end
  end  
end
