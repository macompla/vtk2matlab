%% Single scalar field

%%
% 
% This demo show how to read VTK file with single scalar field and how
% to use Matlab patch function to plot colormap of this field.

clear variables;

%%
% Initialize test in order to have convenient access to test data files.
vtm_test_initialize();
%%
% For convenience we use vtm_test_data_fopen as this function is endowed with
% the knowledge where search for test data. This is not mandatory as vtm.read_vtk
% can by called just with file name.
fhandle = vtm_test_data_fopen('single_scalar_field.vtk');

[nodes, elems, types, fields, info] = vtm.read_vtk(fhandle);
faces = cell2mat(elems');
patch('Faces', faces(:,2:end), 'Vertices', nodes, ...
      'FaceVertexCData',fields.temp.data,'FaceColor','interp')

%%
% Internal demo management
vtm_manage_demos('report', 'single_scalar_field', true);
