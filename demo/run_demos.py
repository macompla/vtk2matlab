"""Driver to publish vtk2matlab demos in HTML tests"""

import os
import socket
import sys
import re

if sys.version_info[1] > 4:
  from subprocess import run, PIPE
else:
  import subprocess

import argparse
sys.path.append("../python")
from vtm.utils import *

def setup_parser():
  parser = argparse.ArgumentParser(description='Run combMESBMRS unit tests')
  parser.add_argument('--filter',
                      dest='filter', 
                      default='.*',
                      help='Regular expression to filter demos to be run',
                     )
  parser.add_argument('--demodir',
                      dest='demodir', 
                      default='',
                      help='Directory in which to run demo',
                     )
  return parser


def get_matlab_demo_args(args):
  """Return command line for running matalb.
  """
  if socket.gethostname() == 'jinx':
    matlabCommand = 'matlab-2017a'
  else:
    matlabCommand = 'matlab'

  args = [matlabCommand,
          '-nosplash', 
          '-nodesktop', 
          '-minimize',
          '-r', 
          'vtm_run_demos()', 
          '-logfile',
          'demogen.log', 
          '-wait'
         ]
  return args


if __name__ == '__main__' :
  parser = setup_parser();

  args = parser.parse_args()
  
  matlab_args = get_matlab_demo_args(args)
  
  rootDir = os.path.join(os.getcwd(), '..')

  print(matlab_args)
  result = run(matlab_args, stdout=PIPE, stderr=PIPE, timeout=1200)
  echo_file('demogen.log')
  exit(result.returncode)
