%% Demos
%
%% Demo execution status
fprintf('Demos executed at: %s\n\n', datestr(now))
vtm_manage_demos('print')
%% Dummy demo
% <html>
% <ul>
% <li><a href="demo_hello_world.html">demo_hello_world</a> Just demonstration that demos work</li>
% </ul>
% </html>
%
%% Scalar field demos
% <html>
% <ul>
% <li><a href="demo_single_scalar_field.html">demo_single_scalar_field</a> Show how to read VTK mesh with single scalar field</li>
% </ul>
% </html>
