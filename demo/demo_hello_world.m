%% Hello World demo

%% Demo template
% 
% This demo is just a template.

clear variables;

%%
% Here is the demo action
disp('Hello World')

%%
% Internal demo management
vtm_manage_demos('report', 'hello_world', true);
