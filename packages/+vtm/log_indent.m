function log_indent( width )
% log_indent : adds given width to global variale vtm_LOG_INDENT
  global vtm_LOG_INDENT
  vtm_LOG_INDENT = vtm_LOG_INDENT + width;
  if vtm_LOG_INDENT < 0
    vtm_LOG_INDENT = 0;
  end
end  
