function putlog(format, varargin)
% putlog - write log message to standard output indenting it according to
% value of vtm_LOG_INDENT.
  global vtm_LOGGING
  global vtm_LOG_INDENT
  if vtm_LOGGING 
      msg = sprintf(format, varargin{:});
      s = sprintf('\n%*s%s\n', 5+vtm_LOG_INDENT, 'LOG: ', msg);
      fprintf(s);
  end
end

