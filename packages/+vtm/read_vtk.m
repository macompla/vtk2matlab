function [nodes, elements, types, fields, info] = readVTK(fid_or_name)
% read_vtk - read data from VTK file.
%
%   [nodes, elements, types, fields, info] = read_vtk(pathOrHandle);
%   nodes - array of node coordinates:  nnodes x 3 
%   elements - array of elements connectivity  nelems x nodes_per_element
%   fields - structure containing arrays of filed data
%   info - structure with information about data read from file:
%       - numOfNodes
%       - numOfElems
%       - title
%       - name of fileds
%       - element type
  needclose = false;
  if ischar(fid_or_name)
    fid = fopen(fid_or_name, 'r');
    needclose = true;
  else
    fid = fid_or_name;
  end
  if( fid==-1 )
    error('Can''t open the file at: %s', fopen(fid));
  end

  str = fgets(fid);   % -1 if eof
  if ~strcmp(str(3:5), 'vtk')
    error('The file is not a valid VTK one.');    
  end

  title = get_title(fid);
  format = get_format(fid);

  tline = read_until_line(fid, 'DATASET')
  dataset = get_dataset(tline);
  
  tline = read_until_line(fid, 'POINTS');
  info.numOfNodes = get_num_of_nodes(tline);
  [nodes, ncount] = fscanf(fid, '%f', [3,info.numOfNodes]);
  if ncount ~= 3*info.numOfNodes
    error('Cannot read vertices from file: %s', fopen(fid));
  end
  nodes = nodes';

  tline = read_until_line(fid, 'CELLS')
  info.numOfElements = get_num_of_elems(tline);
  elements = cell(1, info.numOfElements);
  for i=1:info.numOfElements
    tline = fgetl(fid);
    nnodes = sscanf(tline, '%d');
    data = int32(sscanf(tline, '%d', [1, inf]));
    elements{i} = [data(1), data(2:end)+1];
  end

  tline = read_until_line(fid, 'CELL_TYPES')
  types = fscanf(fid, '%d', [1, info.numOfElements]);

  tline = read_until_line(fid, 'POINT_DATA')

  fields = struct();
  info.fieldnames = {};
  while 1
    [tline, eof] = read_until_line(fid, 'SCALARS|VECTORS|TENSORS')
    if eof
      break
    end
    [fieldType, fieldName] = get_field_type_name(tline);
    if strcmp(fieldType, 'SCALARS')
      ncomponents = get_num_of_scalar_components(tline);
      data = read_scalars(fid, info.numOfNodes, ncomponents);
    elseif strcmp(fieldType, 'VECTORS')
      data = read_vectors(fid, info.numOfNodes);
    else
      error('Unsuported field type: %s in file %s', fieldType, fopen(fid));
    end
    fields.(fieldName) = struct('type', fieldType, 'data', data);
  end

  info.fieldnames = fieldnames(fields)
  if needclose
    fclose(fid);
  end
end

function [dstype] = get_dataset(tline)
  tokens = split(tline);
  dstype = tokens{2}; 
end

function [format] = get_format(fid)
  format=fgetl(fid);
end

function [num_of_nodes] = get_num_of_nodes(tline)
  tokens = split(tline);
  num_of_nodes = sscanf(tokens{2}, '%d');
end

function [ncomponents] = get_num_of_scalar_components(tline)
  tokens = split(tline)
  ncomponents = 1
  if length(tokens) > 3
    ncomponents = tokens{4};
  end
end

function [type, name] = get_field_type_name(tline)
  tokens = split(tline);
  type = tokens{1};
  name = tokens{2};
end

function [data] = read_scalars(fid, nnodes, ncomponents)
  tline = read_until_line(fid, 'LOOKUP_TABLE')
  tokens = split(tline);
  if ~strcmp(tokens{2}, 'default')
    error('Readig non default lookup table is not supported')
  end
  data = fscanf(fid, '%f', [ncomponents, nnodes]);
  data = data';
end

function [data] = read_vectors(fid, nnodes)
  tokens = split(tline);
  if ~strcmp(tokens{2}, 'default')
    error('Readig non default lookup table is not supported')
  end
  data = fscanf(fid, '%f', [3, nnodes]);
  data = data';
end

function [title] = get_title(fid)
  title = fgetl(fid);
end

function [num_of_elems] = get_num_of_elems(tline)
  tokens = split(tline);
  num_of_elems = sscanf(tokens{2}, '%d');
end

function [tline, eof] = read_until_line(fid, pattern)
  eof=false;
  while 1
    tline = fgetl(fid);
    if ~ischar(tline)
      tline='';
      eof=true;
      return
    else
      if regexp(tline, pattern)
        return
      end 
    end
  end
end
