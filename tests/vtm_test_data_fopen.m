%% Open test data file for reading.
% The filenae should be path relative to tests/data folder 
function [fid] = vtm_test_data_fopen(filename)
  global vtm_TEST
  path = fullfile(vtm_TEST.datadir, filename);
  fid = fopen(path, 'r');
end 
