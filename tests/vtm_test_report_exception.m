%% Writes exception report to give fid
function vtm_test_report_exception(fid, exception)
  if vtm.is_octave()
    for frame = exception.stack
      fprintf(fid, '    where: %s\n', frame.name)
    end
  else
    fprintf(fid, '%s\n', getReport(exception))
  end 
end
