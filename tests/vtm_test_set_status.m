function vtm_test_set_status(status)
% Set the status of current test
  global vtm_TEST
  if ~strcmp(vtm_TEST.current, '')
    vtm_TEST.retistry.(vtm_TEST.current).('status') = status;
    fprintf(vtm_TEST.statusfid, ' %s ', status);
  end
end
