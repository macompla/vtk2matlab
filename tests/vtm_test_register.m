function vtm_test_register(name, brief)
% Register a test in unit tests framework.  
  global vtm_TEST
  test.brief = brief;
  test.status = 'RUNNING';
  vtm_TEST.registry.(name) = test;
  vtm_TEST.current = name;
  fprintf(vtm_TEST.statusfid, '\n%s ... ', name);
  cd(vtm_TEST.testdir)
end
