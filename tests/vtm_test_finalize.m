function vtm_test_finalize()
  global vtm_TEST
  ntests = length(fieldnames(vtm_TEST.registry));
  fprintf(vtm_TEST.summaryfid, 'Run %d tests\n', ntests);
  fclose(vtm_TEST.summaryfid);
end
