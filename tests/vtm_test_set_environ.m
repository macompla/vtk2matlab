function [mypathstr] = vtm_test_set_environ()
% vtm_set_test_environ - set environment to make possible running tests.
% This account to adding paths for Matlab to find required m-files
% of combMESBMRS code. 
%
% Return the path to the folder in which source code of this 
% function is located.
  persistent pth
  if isempty(pth)
    sep = filesep();
    mypath = mfilename('fullpath');
    [pth,~,~] = fileparts(mypath);
  else
    mypathstr = pth;  
    return
  end
  mypathstr = pth;  
  % Essential bootstrap code
  codeFolders = {'packages';
                 'tests';
                 'demo'
                };
  codeFolders = strcat(mypathstr, sep, '..', sep, codeFolders);
  addpath(codeFolders{:})

  % Other folders 
  codeFolders = {fullfile('external', 'rgb');
                };
  codeFolders = strcat(mypathstr, sep, '..', sep, codeFolders);
  addpath(codeFolders{:})

  if vtm.is_octave()
    portingFolder = fullfile(mypathstr, '..', 'utils', 'porting', 'octave');
    addpath(portingFolder); 
  end
end
