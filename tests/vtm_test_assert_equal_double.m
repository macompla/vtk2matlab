function vtm_test_asssert_equal(expected, actual, epsilon, varargin)
% Test if actuall is equal expected. If not raise
% error. Write appropriate message.
  esize = size(expected);
  asize = size(actual);
  details = '  Details : none';
  if ~isempty(varargin)
    details = sprintf('  Details : %s', varargin{:});
  end
  if esize ~= asize
    vtm_test_set_status('FAILURE')
    error('Test assertion FAILED: size does not match\n%s', details)
  end 
  if abs(expected-actual) > epsilon
    estr = sprintf(' Expected : %s\n',vtm.string(expected));
    astr = sprintf('   Actual : %s\n',vtm.string(actual));
    vtm_test_set_status('FAILURE')
    error('Test assertion FAILED:\n%s\n%s\n%s\n', estr, astr, details)
  else
    vtm_test_set_status('OK')
  end  
end                
