function vtm_test_assert_equal(expected, actual, varargin)
% Test if actuall is equal expected. If not raise
% error. Write appropriate message.
  if ~isequal(expected, actual)
    estr = sprintf(' Expected : %s\n',vtm.to_string(expected));
    astr = sprintf('   Actual : %s\n',vtm.to_string(actual));
    vtm_test_set_status('FAILURE')
    details = '  Details : none';
    if ~isempty(varargin)
       details=sprintf('  Details : %s', varargin{:});
    end
    error('Test assertion FAILED:\n%s\n%s\n%s', estr, astr, details)
  else
    vtm_test_set_status('OK')
  end  
end                
