function vtm_run_tests(varargin)
% vtm_run_test : run all scripts in tests directory that match the pattern
%               test_*.m
% Arguments
%   options - structure with the following fiels:
%     exitAfter - boolean, if true call exit fater running tests
%     filter - string, regular expression for selecting tests to run
  if nargin > 0
    options = varargin{:};
  else
    options.exitAfter = false;
    options.filter = '.*';
  end

  if ~isfield(options, 'exitAfter')
    options.exitAfter = false;
  end

  if ~isfield(options, 'filter')
    options.filter = '.*';
  end

  if ~isfield(options, 'suite')
    options.suite = '';
  end

  exitCode=0;
  try
    mypathstr = vtm_test_set_environ();
    vtm.setup_logging(true);
 
    sep = filesep();
    % Initialize unit testing framework
    vtm_test_initialize(options)

    suitePath = 'suites';
    if ~isempty(options.suite)
       suitePath = fullfile(suitePath, options.suite);
    end 
    tests = vtm.files_recursively(fullfile(mypathstr, suitePath), '.m');

    for i=1:length(tests)
       testname = tests(i).basename;
       if isempty(regexp(testname, options.filter, 'once'))
         continue
       end 
       try 
         vtm_test_run_shilded(tests(i).path)
         fprintf(1, 'PASSED %s\n', testname) 
       catch exception
         fprintf(1, 'FAILED %s\n', testname) 
         fprintf(1, '  Reason: %s\n', exception.message)
         vtm_test_report_exception(1, exception)
         exitCode=2;
       end
    end
  catch exception
    disp('Exception catched in vtm_run_tests')
    disp(exception.message)
    exitCode=22;
  end
 
  vtm_test_finalize()
 
  if options.exitAfter
    exit(exitCode)
  end   
end
