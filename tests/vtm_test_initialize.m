function vtm_test_initialize(options)
% Do preparatory steps to run test suite
  global vtm_TEST
  vtm_TEST.statusfid = fopen('teststatus.log', 'w');
  vtm_TEST.summaryfid = fopen('testsummary.log', 'w');
  vtm_TEST.registry = struct();
  vtm_TEST.current = '';

  mypath = mfilename('fullpath');
  [mypath,~,~] = fileparts(mypath);

  vtm_TEST.datadir = fullfile(mypath, 'data');

  if nargin < 1
    options = struct();
  end
  
  if isfield(options, 'testdir')
    if isempty(fileparts(options.testdir))
      vtm_TEST.testdir = fullfile(pwd, options.testdir);
    else
      vtm_TEST.testdir = options.testdir;
    end    
  else
    vtm_TEST.testdir = tempdir();
  end
  vtm_TEST.testdir=strrep(vtm_TEST.testdir, '\', '\\');

  if 0 == exist(vtm_TEST.testdir, 'dir')
    mkdir(vtm_TEST.testdir);
  end

  if 7 ~= exist(vtm_TEST.testdir, 'dir')
    error('Cannot access tests directory')
  end
  msg = sprintf('Test directory set to : %s', strrep(vtm_TEST.testdir, '\', '\\'));
  vtm.putlog(msg)
end  
